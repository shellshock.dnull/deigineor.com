---
author: "shellshock"
title: "Deigineor - Arkan"
date: "2022-06-05"
description: "Deigineor - Arkan"
image: "img/arkan.jpg"
tags: [
    "guitar",
    "folk",
]
categories: [
    "folk"
]
archives: ["2022/06"]
---

## Spotify
{{< spotify "track/4oxE8yzkhVluKbQkcOoqee?si=8a045e9350a64a18" >}}

## YouTube
{{< youtube TaLwf3MBl4w >}}

## Apple Music / iTunes
{{< itunes "us/album/arkan/1628016185?i=1628016366" >}}

## More platforms [here](https://distrokid.com/hyperfollow/deigineor/arkan)
