---
author: "Hugo Authors"
title: "Deigineor - Assimilation" 
date: "2022-06-16"
description: "Deigineor - Assimilation" 
image: "img/assimilation.jpg"
tags: [
    "electronic",
    "cyberwave"
]
categories: [
    "electronic"
]
archives: ["2022/06"]
---

## Spotify
{{< spotify "track/1vcwuvec2OdxSiGZDeJJ5S?si=e7b9543930a14377" >}}

## YouTube

{{< youtube v-U2cGqPp4A >}}

## Apple Music / iTunes
{{< itunes "us/album/assimilation/1630202881?i=1630202883" >}}


## More platforms [here](https://distrokid.com/hyperfollow/deigineor/assimilation)
