---
author: "Hugo Authors"
title: "Deigineor - Polaris"
date: "2020-10-25"
description: "Deigineor - Polaris"
image: "img/polaris.jpg"
tags: [
    "lyrics",
    "chill"
]
categories: [
    "lyrics"
]
archives: ["2020/10"]
---

## Spotify

{{< spotify "track/6XTtGsD2nsjB9miRWeGvVY?si=76e8699fd09b4126" >}}

## YouTube

{{< youtube m-oZrwvae4A >}}

## Apple Music / iTunes

{{< itunes "us/album/polaris-single/1537311074" >}}

## More platforms [here](https://distrokid.com/hyperfollow/deigineor/polaris)
