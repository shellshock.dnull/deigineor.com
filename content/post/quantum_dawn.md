---
author: "shellshock"
title: "Deigineor - Quantum Dawn"
date: "2024-09-19"
description: "Deigineor - Quantum Dawn"
image: "img/quantum_dawn.jpg"
tags: [
    "ost",
    "cinematic",
    "soundtrack",
]
categories: [
    "soundtrack"
]
archives: ["2024/09"]
---

## Spotify
{{< spotify "album/3qvDt5U2J10VnOxMBoGTx4?si=kSbbu5XuS5aPWGLbfyP_qw" >}}


## YouTube
{{< youtube X0fBZbIFTXo >}}


## Apple Music / iTunes
{{< itunes "ca/album/quantum-dawn-original-game-soundtrack/1769779862" >}}


## More platforms [here](https://distrokid.com/hyperfollow/deigineor/quantum-dawn-original-game-soundtrack)
