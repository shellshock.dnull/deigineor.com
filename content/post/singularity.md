---
author: "Hugo Authors"
title: "Deigineor - Singularity" 
date: "2023-06-16"
description: "Deigineor - Singularity" 
image: "img/singularity.jpg"
tags: [
    "electronic",
    "cyberwave"
]
categories: [
    "electronic"
]
archives: ["2023/06"]
---

## Spotify
{{< spotify "album/64qQ4ZeBs6gkHAY7TxHCOw?si=vaC0C9qJS1iF_dAXfM3IKA" >}}

## YouTube
{{< youtube "JmKlt4RWjDk" >}}

## Apple Music / iTunes
{{< itunes "us/album/singularity/1692998231" >}}


## More platforms [here](https://distrokid.com/hyperfollow/deigineor/singularity)
