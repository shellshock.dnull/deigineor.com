---
author: "shellshock"
title: "Deigineor - Nongrad"
date: "2022-05-11"
description: "Deigineor - Nongrad"
image: "img/nongrad.jpg"
tags: [
    "electronic",
    "cyberwave",
]
categories: [
    "electronic"
]
archives: ["2022/05"]
---

## Spotify

{{< spotify "track/3e72QoizrskHbwTsGCcoXv?si=643c2c6c579143e5" >}}

## YouTube

{{< youtube Qi9v8yQDHD4 >}}

## Apple Music / iTunes

{{< itunes "us/album/nongrad/1623569448?i=1623569449" >}}


## More platforms [here](https://distrokid.com/hyperfollow/deigineor/nongrad)
