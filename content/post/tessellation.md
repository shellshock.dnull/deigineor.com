---
author: "shellshock"
title: "Deigineor - Tessellation"
date: "2022-05-08"
description: "Deigineor - Tessellation"
image: "img/tessellation.jpg"
tags: [
    "electronic",
    "house",
]
categories: [
    "electronic"
]
archives: ["2022/05"]
---

## Spotify

{{< spotify "album/2V9MIT3dbL7DVJvjvS6W1p?si=AnhiEcH4TuqZODWgU3fW8w" >}}


## YouTube

{{< youtube bQ2laJQtQNM >}}



## More platforms [here](https://distrokid.com/hyperfollow/deigineor/tessellation)


