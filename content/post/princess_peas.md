---
author: "shellshock"
title: "Deigineor - Princess' Peas"
date: "2022-07-29"
description: "Deigineor - Princess' Peas"
image: "img/princess_peas.jpg"
tags: [
    "ost",
    "cinematic",
    "soundtrack",
]
categories: [
    "soundtrack"
]
archives: ["2022/07"]
---

## Spotify
{{< spotify "album/2Dj4FpLCRSfwiq84FcYHkY?si=jUzzN0jdQ2qcK8dta5uW3Q" >}}

## YouTube
{{< youtube 4YLqYt4s0Jk >}}


## Apple Music / iTunes
{{< itunes "us/album/princess-peas-original-game-soundtrack/1637498849" >}}


## More platforms [here](https://distrokid.com/hyperfollow/deigineor/princess-peas-original-game-soundtrack)
