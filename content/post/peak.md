---
author: "shellshock"
title: "Deigineor - Peak"
date: "2024-04-07"
description: "Deigineor - Peak"
image: "img/peak.jpg"
tags: [
    "electronic",
    "cyberwave",
]
categories: [
    "electronic"
]
archives: ["2024/04"]
---

## Spotify
{{< spotify "track/3TuIm2Ecy7VGdu9kAUy91i?si=188071c5055b4349" >}}


## YouTube
{{< youtube Uq9rDL24wqM >}}


## Apple Music / iTunes
{{< itunes "us/album/peak/1740034608?i=1740034609" >}}


## More platforms [here](https://distrokid.com/hyperfollow/deigineor/peak)
