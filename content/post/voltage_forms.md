---
author: "Hugo Authors"
title: "Deigineor - Voltage Forms"
date: "2022-04-27"
description: "Deigineor - Voltage Forms"
image: "img/voltage_forms.jpg"
tags: [
    "electronic",
    "cyberwave"
]
categories: [
    "electronic"
]
archives: ["2022/04"]
---

## Spotify
{{< spotify "track/5vdfY7JYXO8RE6khUqkKgE?si=-IgHT164S3a-JKEeqhbqdQ" >}}

## YouTube

{{< youtube XmzZtXQ5lRU >}}

## Apple Music / iTunes
{{< itunes "us/album/voltage-forms/1621266190?i=1621266191" >}}


## More platforms [here](https://distrokid.com/hyperfollow/deigineor/voltage-forms)
