---
author: "Hugo Authors"
title: "Deigineor - Places"
date: "2020-10-11"
description: "Deigineor - Places"
image: "img/places.jpg"
tags: [
    "guitar",
]
categories: [
    "guitar"
]
archives: ["2020/10"]
---

## Spotify

{{< spotify "track/1PLYjOER6xJwKqR5XxOkKn?si=58680739401a4b23" >}}


## YouTube

{{< youtube oktO5I4PdYw >}}

## Apple Music / iTunes

{{< itunes "us/album/places-single/1539998485" >}}

## More platforms [here](https://distrokid.com/hyperfollow/deigineor/places)
