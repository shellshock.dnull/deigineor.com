---
author: "Hugo Authors"
title: "Deigineor - Grains"
date: "2020-10-03"
description: "Deigineor - Grains"
image: "img/grains.jpg"
tags: [
    "trap",
    "electronic",
]
categories: [
    "electronic"
]
archives: ["2020/10"]
---

## Spotify

{{< spotify "track/73j0VbmvniJRls1iir7rOk?si=9844ff38f50240b1" >}}

## YouTube

{{< youtube PB5mOIAAk0Y >}}

## Apple Music / iTunes

{{< itunes "us/album/grains-single/1538715529" >}}


## More platforms [here](https://distrokid.com/hyperfollow/deigineor/grains)


