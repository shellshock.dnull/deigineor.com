---
author: "shellshock"
title: "Deigineor - Decay"
date: "2022-05-18"
description: "Deigineor - Decay"
image: "img/decay.jpg"
tags: [
    "rock",
    "electronic",
]
categories: [
    "Guitar"
]
archives: ["2022/05"]
---

## Spotify

{{< spotify "track/3htZeoy6f3J44PFH1Euiea?si=0be56fe461db4a98" >}}


## YouTube

{{< youtube z-4dw5O3OxQ >}}

## Apple Music / iTunes

{{< itunes "us/album/decay/1624875888?i=1624875890" >}}


## More platforms [here](https://distrokid.com/hyperfollow/deigineor/decay)
