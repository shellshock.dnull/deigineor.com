---
author: "shellshock"
title: "Deigineor - Precision"
date: "2024-02-15"
description: "Deigineor - Precision"
image: "img/precision.jpg"
tags: [
    "electronic",
    "cyberwave",
]
categories: [
    "electronic"
]
archives: ["2024/02"]
---

## Spotify
{{< spotify "track/7axgzbsHmFFBaxlEsGS4rt?si=ff0f1e2d5c1f4520" >}}


## YouTube
{{< youtube 0wv9g_T5UnM >}}


## Apple Music / iTunes
{{< itunes "us/album/precision/1731589446?i=1731589448" >}}


## More platforms [here](https://distrokid.com/hyperfollow/deigineor/precision)
