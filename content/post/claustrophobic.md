---
author: "Hugo Authors"
title: "Deigineor - Claustrophobic"
date: "2022-04-18"
description: "Deigineor - Claustrophobic"
image: "img/claustrophobic.jpg"
tags: [
    "instrumental",
    "industrial",
    "metal",
]
categories: [
    "heavy"
]
archives: ["2022/04"]
---

## Spotify

{{< spotify "track/3W4UP9LZVANXMFNVxTipjC?si=012ff173bf934c89" >}}


## YouTube

{{< youtube BiPl3X8nQqo >}}

## Apple Music / iTunes

{{< itunes "us/album/claustrophobic/1619783353?i=1619783354" >}}


## More platforms [here](https://distrokid.com/hyperfollow/deigineor/claustrophobic)
