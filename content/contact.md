---
title: "Contact"
description: "Contact to stay tuned"
date: 2022-04-23
aliases: ["contact"]
author: "Hugo Authors"
---

Find music on your favourite platform:
- [*youtube*](https://www.youtube.com/channel/UC47vqawmWVk7IHtbcYqYBNw)
- [*spotify*](https://open.spotify.com/artist/0nK8PxMNI2dpZ3SSPchHRB)
- [*deezer*](https://www.deezer.com/us/artist/111255102)
- [*apple music*](https://music.apple.com/us/artist/deigineor/1537308317)
- [*napster*](https://us.napster.com/artist/deigineor)
- [*amazon music*](https://www.amazon.com/s?k=Deigineor&i=digital-music&search-type=ss&ref=ntt_srch_drd_B08NCN1RPY)

Contact to stay tuned:
* [*facebook*](https://www.facebook.com/DEIGINEOR/)
* [*instagram*](https://www.instagram.com/deigineor)
* [*telegram*](https://t.me/deigineor_official)
